## Version 3.3.0
- added optional Bus configuration
## Version 3.2.0
- updated FROST-Server
- added new FROST-Server settings to reflect release 2.0-branch
- internal http-deployment is using nginx-ingress now

## Version 3.1.0
- updated integrated nginx-ingress to helm chart version 4.1.1

## Version 3.0.0
- added internal (subnet-only) FROST-Server http deployment
- added rate limit configutration for external http

## Version 2.1.0
- added nginx ingress autoscaling and ressources parameters
- activated longer timeouts for websocket connections
- added setting to enable/disable nginx default backend
- added configurable ingress class admissionwebhook and ingressClassResource
- added ingressClass from Values
- implemeneted setting to enable/disable liveness and readiness probes
- nginx ingress dependency activated
- ingress updated to 3.34.0
- fixed:wrong service name for nginx ingress
- fix: resources values path
- openapi plugin and customLink extension user configurable
- configurable resource requests and limits added
- removed old nginx-ingress
- updated Ingress ressource definition to apiVersion: networking.k8s.io/v1
- updated nginx-ingress to v3.34.0
- added ingressClass
- fixed ingress enabled bug
- deleted redundant http ingress
- fixed websocket ingress name
## Version 2.0.0

- version 1.0.0 was released on 2021-05-26
- updated FROST-Server http and mqtt-module to Version 1.13.1
- minimum Kubernetes Version is now 1.16.x
- removed Keycloak and cert-manager integrated helm charts (they need to installed individually now from their respective helm repositories, see [Readme](./Readme.md) for details)

## Version 1.0.1

- renamed repository to "udh-hh-iot"
- repo contains now only this one helm chart

## Release Version 1.0.0

- Version 1.0.0 was released on 2020-03-12.
