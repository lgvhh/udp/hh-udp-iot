{{- if .Values.frost.httpInternal.enabled -}}
{{- $tier := "http-internal" -}}
{{- $fullName := include "frost-server.fullName" (merge (dict "tier" $tier) .) -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $fullName }}
  labels:
    app.kubernetes.io/managed-by: {{ .Release.Service }}
    app.kubernetes.io/instance: {{ .Release.Name }}
    helm.sh/chart: {{ include "frost-server.chart" . }}
    app: {{ include "frost-server.name" . }}
    component: {{ $tier }}
spec:
  selector:
    matchLabels:
      app.kubernetes.io/managed-by: {{ .Release.Service }}
      app.kubernetes.io/instance: {{ .Release.Name }}
      app: {{ include "frost-server.name" . }}
      component: {{ $tier }}
  {{- if not .Values.frost.httpInternal.hpa.enabled }}
  replicas: {{ .Values.frost.httpInternal.replicas }}
  {{ end }}
  template:
    metadata:
      labels:
        app.kubernetes.io/managed-by: {{ .Release.Service }}
        app.kubernetes.io/instance: {{ .Release.Name }}
        helm.sh/chart: {{ include "frost-server.chart" . }}
        app: {{ include "frost-server.name" . }}
        component: {{ $tier }}
    spec:
      containers:
        - name: {{ $fullName }}
          image: "{{ .Values.frost.httpGlobal.image.registry }}/{{ .Values.frost.httpGlobal.image.repository }}:{{ .Values.frost.httpGlobal.image.tag }}"
          imagePullPolicy: {{ .Values.frost.httpGlobal.image.pullPolicy | quote }}
          resources:
            requests:
              cpu: {{ .Values.frost.httpInternal.resources.requests.cpu }}
              memory: {{ .Values.frost.httpInternal.resources.requests.memory }}
            limits:
              cpu: {{ .Values.frost.httpInternal.resources.limits.cpu }}
              memory: {{ .Values.frost.httpInternal.resources.limits.memory }}
          ports:
            - name: tomcat
              containerPort: 8080
          {{- if .Values.frost.httpInternal.livenessProbe.enabled }}
          livenessProbe:
            httpGet:
              path: {{ .Values.frost.httpInternal.livenessProbe.httpGet.path }}
              port: {{ .Values.frost.httpInternal.livenessProbe.httpGet.port }}
            initialDelaySeconds: {{ .Values.frost.httpInternal.livenessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.frost.httpInternal.livenessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.frost.httpInternal.livenessProbe.timeoutSeconds}}
            successThreshold: {{ .Values.frost.httpInternal.livenessProbe.successThreshold}}
            failureThreshold: {{ .Values.frost.httpInternal.livenessProbe.failureThreshold }}
          {{ end }}
          {{- if .Values.frost.httpInternal.readinessProbe.enabled }}
          readinessProbe:
            httpGet:
              path: {{ .Values.frost.httpInternal.readinessProbe.httpGet.path }}
              port: {{ .Values.frost.httpInternal.readinessProbe.httpGet.port }}
            initialDelaySeconds: {{ .Values.frost.httpInternal.readinessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.frost.httpInternal.readinessProbe.initialDelaySeconds }}
            timeoutSeconds: {{ .Values.frost.httpInternal.readinessProbe.timeoutSeconds}}
            failureThreshold: {{ .Values.frost.httpInternal.readinessProbe.failureThreshold }}
          {{ end }}
          env:
            # Internal properties
            - name: ApiVersion
              value: {{ include "frost-server.httpGlobal.apiVersion" . | quote }}
            - name: serviceRootUrl
              value: {{ include "frost-server.httpInternal.serviceRootUrl" . | quote }}
            - name: enableActuation
              value: "{{ .Values.frost.enableActuation }}"

            # Log related properties
            - name: logSensitiveData
              value: {{ .Values.frost.httpInternal.log.logSensitiveData | quote }}
            - name: queueLoggingInterval
              value: {{ .Values.frost.httpInternal.log.queueLoggingInterval | quote }}
            - name: FROST_LL
              value: {{ .Values.frost.httpInternal.log.FROST_LL }}
            - name: FROST_LL_parser
              value: {{ .Values.frost.httpInternal.log.FROST_LL_parser }}
            - name: FROST_LL_queries
              value: {{ .Values.frost.httpInternal.log.FROST_LL_queries }}
            - name: FROST_LL_requests
              value: {{ .Values.frost.httpInternal.log.FROST_LL_requests }}
            - name: FROST_LL_settings
              value: {{ .Values.frost.httpInternal.log.FROST_LL_settings }}
            - name: FROST_LL_io_moquette
              value: {{ .Values.frost.httpInternal.log.FROST_LL_io_moquette }}
            - name: FROST_LL_liquibase
              value: {{ .Values.frost.httpInternal.log.FROST_LL_liquibase }}
            - name: FROST_LL_org_jooq
              value: {{ .Values.frost.httpInternal.log.FROST_LL_org_jooq }}

            # HTTP related properties
            - name: defaultCount
              value: "{{ .Values.frost.httpInternal.defaultCount }}"
            - name: defaultTop
              value: "{{ .Values.frost.httpInternal.defaultTop }}"
            - name: maxTop
              value: "{{ .Values.frost.httpInternal.maxTop }}"
            - name: maxDataSize
              value: "{{ .Values.frost.httpInternal.maxDataSize | int64 }}"
            - name: useAbsoluteNavigationLinks
              value: "{{ .Values.frost.httpInternal.useAbsoluteNavigationLinks }}"

            {{ if .Values.frost.mqtt.enabled -}}
            # Messages bus related properties
            - name: bus_mqttBroker
              value: {{ printf "tcp://%s:1883" (include "frost-server.fullName" (merge (dict "tier" "bus") .)) | quote }}
            - name: bus_busImplementationClass
              value: "{{ .Values.frost.bus.implementationClass }}"
            - name: bus_topicName
              value: "{{ .Values.frost.bus.topicName }}"
            - name: bus_qosLevel
              value: "{{ .Values.frost.bus.qos }}"
            - name: bus_sendWorkerPoolSize
              value: "{{ .Values.frost.httpInternal.bus.sendWorkerPoolSize }}"
            - name: bus_sendQueueSize
              value: "{{ .Values.frost.httpInternal.bus.sendQueueSize }}"
            - name: bus_recvWorkerPoolSize
              value: "{{ .Values.frost.httpInternal.bus.recvWorkerPoolSize }}"
            - name: bus_recvQueueSize
              value: "{{ .Values.frost.httpInternal.bus.recvQueueSize }}"
            - name: bus_maxInFlight
              value: "{{ .Values.frost.httpInternal.bus.maxInFlight }}"
            - name: mqtt_exposedEndpoints
              value: "{{ .Values.frost.httpInternal.mqttExposedEndpoints }}"
            {{- end }}

            # Persistence related properties
            - name: persistence_db_jndi_datasource
              value: ""
            - name: persistence_db_driver
              value: "org.postgresql.Driver"
            - name: persistence_db_url
              value: {{ printf "jdbc:postgresql://%s:5432/%s" (default (include "frost-server.fullName" (merge (dict "tier" "db") .)) .Values.frost.httpInternal.db.ip) .Values.frost.httpInternal.db.frostConnection.database | quote }}
            - name: persistence_persistenceManagerImplementationClass
              value: "{{ .Values.frost.db.implementationClass }}"
            - name: persistence_idGenerationMode
              value: "{{ .Values.frost.db.idGenerationMode }}"
            - name: persistence_autoUpdateDatabase
              value: "{{ .Values.frost.httpInternal.db.autoUpdate }}"
            - name: persistence_alwaysOrderbyId
              value: "{{ .Values.frost.httpInternal.db.alwaysOrderbyId }}"
            - name: persistence_db_conn_max
              value: "{{ .Values.frost.httpInternal.db.maximumConnection }}"
            - name: persistence_db_conn_idle_max
              value: "{{ .Values.frost.httpInternal.db.maximumIdleConnection }}"
            - name: persistence_db_conn_idle_min
              value: "{{ .Values.frost.httpInternal.db.maximumIdleConnection }}"
            - name: persistence_queryTimeout
              value: {{ .Values.frost.httpInternal.db.queryTimeout | quote }}
            - name: persistence_slowQueryThreshold
              value: {{ .Values.frost.httpInternal.db.slowQueryThreshold | quote }}
            - name: persistence_db_username
              valueFrom:
                secretKeyRef:
                  name: {{ include "frost-server.fullName" . }}-http-internal
                  key: db.username
            - name: persistence_db_password
              valueFrom:
                secretKeyRef:
                  name: {{ include "frost-server.fullName" . }}-http-internal
                  key: db.password
            - name: persistence_countMode
              value: "{{ .Values.frost.httpInternal.persistence.countMode }}"
            - name: persistence_countEstimateThreshold
              value: "{{ .Values.frost.httpInternal.persistence.countEstimateThreshold }}"
            #
            # Extensions
            - name: extension_customLinks_enable
              value: "{{ .Values.frost.httpInternal.extensions.customLinks.enable }}"
            - name: extension_customLinks_recurseDepth
              value: "{{ .Values.frost.httpInternal.extensions.customLinks.recurseDepth }}"
            - name: extension_filterDelete_enable
              value: "{{ .Values.frost.httpInternal.extensions.filterDelete.enable }}"
            #
            # Plugins
            - name: plugins_odata_enable
              value: "{{ .Values.frost.httpInternal.plugins.odata.enable }}"
            - name: plugins_openApi_enable
              value: "{{ .Values.frost.httpInternal.plugins.openApi.enable }}"
            #
            # authentication related properties
            {{- if .Values.frost.httpInternal.enableAuthentification }}
            - name: auth_provider
              value: "{{ .Values.frost.httpInternal.auth.provider }}"
            - name: auth_allowAnonymousRead
              value: "{{ .Values.frost.httpInternal.auth.allowAnonymousRead }}"
            - name: auth_role_read
              value: "{{ .Values.frost.httpInternal.auth.roleRead }}"
            - name: auth_role_create
              value: "{{ .Values.frost.httpInternal.auth.roleCreate }}"
            - name: auth_role_update
              value: "{{ .Values.frost.httpInternal.auth.roleUpdate }}"
            - name: auth_role_delete
              value: "{{ .Values.frost.httpInternal.auth.roleDelete }}"
            - name: auth_role_admin
              value: "{{ .Values.frost.httpInternal.auth.roleAdmin }}"
            - name: auth_keycloakConfigUrl
              value: "{{ .Values.frost.httpInternal.auth.keycloakConfigUrl }}"
            - name: auth_keycloakConfigSecret
              value: "{{ .Values.frost.httpInternal.auth.keycloackConfigSecret }}"
            {{- end }}
{{- end -}}