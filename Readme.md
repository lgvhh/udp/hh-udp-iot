# HH-UDP-IoT repository and Cookbook

## Introduction

This is the Hamburg Urban Data Platform - IoT Helm chart (HH-UDP-IoT) repository and cookbook. It provides components for the real time data infrastructure of the Urban Data Platform.

The cookbook comprises a guide to deploy  a [**FR**aunhofer **O**pensource **S**ensor**T**hings-Server (FROST-Server)](https://www.iosb.fraunhofer.de/servlet/is/80113/) along with Keycloak, an identity provider for FROST, and utility applications to provide an integrated OGC SensorThings API conform, production-ready (TLS-encrypted) environment into your Kubernetes cluster.

The FROST-Server is the first complete, open-source implementation of the OGC SensorThings API standard. Whereas [Keycloak](https://developers.redhat.com/blog/2019/12/11/keycloak-core-concepts-of-open-source-identity-and-access-management/) is an OpenIDConnect-implementation for identity and access management developed by Red Hat. It is used to administer user roles (e.g. read, create, update) and to manage users with different access privileges to the FROST-Server.

For production ready usage this chart installs a [NGINX-Ingress](https://kubernetes.github.io/ingress-nginx/) resource to connect your cloud-provider based Loadbalancers DNS and IP addresses to the FROST-Server and Keycloak instances. Client-Server connections are encrypted by default. The [jetstack cert-manager](https://cert-manager.io/docs/) provides and updates TLS certificates from Let's Encrypt fully automatic.

This Readme provides you with the basic setup and installation steps necessary to install the HH-UDP-IoT Helm chart into your Kubernetes cluster.

* [HH-UDP-IoT repository and Cookbook](#hh-udp-iot-repository-and-cookbook)
  * [Introduction](#introduction)
  * [EU-funding notice](#eu-funding-notice)
  * [Prerequisites](#prerequisites)
  * [Setup steps](#setup-steps)
    * [Add Helm Repositories](#add-helm-repositories)
    * [Deploy the cert manager Helm Chart](#deploy-the-cert-manager-helm-chart)
    * [Codecentric Keycloakx installation steps](#codecentric-keycloakx-installation-steps)
    * [Keycloak Realm and Client configuration](#keycloak-realm-and-client-configuration)
    * [FROST-Server setup and installation](#frost-server-setup-and-installation)
      * [Create and prepare a database for the FROST-Server](#create-and-prepare-a-database-for-the-frost-server)
      * [Deploy the HH-UDP-IoT Helm Chart](#deploy-the-hh-udp-iot-helm-chart)
      * [do some checks after the installation](#do-some-checks-after-the-installation)
    * [[Optional] Configure streaming-replication](#optional-configure-streaming-replication)
  * [Notes](#notes)
    * [Lets Encrypt rate limits](#lets-encrypt-rate-limits)
    * [Local Helm chart deployment](#local-helm-chart-deployment)
    * [Known issues](#known-issues)
  * [Source and documentation pages](#source-and-documentation-pages)
    * [About Hamburg Urban Data Platform](#about-hamburg-urban-data-platform)
    * [General documentation sites](#general-documentation-sites)
    * [Keycloak REALM-configuration for FROST-Server](#keycloak-realm-configuration-for-frost-server)
  * [License information](#license-information)

## EU-funding notice

|||
|--|--|
|
<img src="pics/flag_yellow_low.jpg" alt="drawing" width="180"/> | This Repository is part of a project that has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 731297.|

## Application description

The HH-UDP-IoT helm chart deploys the three FROST-Server main components HTTP, MQTT and Bus to your Kubernetes-Cluster. For detailed information about its architecture please consult the [official FROST-Servr documentation page](https://fraunhoferiosb.github.io/FROST-Server/).

With our helm chart the HTTP-component can optionally be deployed twice. This is reasonable if you have more than one database servers, one main and one or more additional replicas. By connecting your public DNS to the external http-deployment, it can be used exclusively for read access. Whereas the internal http-deployment can be used for writing to your database and is connected with the primary DB-Server. Below is a diagram displaying the application structure. Dashed components are optional

![app](pics/hh-udp-iot-helm-chart-v3-2-x.svg "application-structure")
*HH-UDP-IoT FROST-Server components and connections*


## Prerequisites

* Working Kubernetes cluster, Version >=1.23.x (later versions my work but are not yet tested)
    * [AZURE AKS](https://docs.microsoft.com/en-us/azure/aks/tutorial-kubernetes-deploy-cluster)
    * [AWS EKS](https://aws.amazon.com/en/eks/getting-started/)
    * [Google Cloud Kubernetes Engine](https://cloud.google.com/kubernetes-engine)
    * [Google Cloud Kubernetes Engine](https://cloud.google.com/,kubernetes-engine), or
    * [MicroK8s](https://microk8s.io/)
* Two external IP-Adresses with two DNS-entries (for the KeyCloak endpoint and for the Frost-Server)
* [kubectl](https://kubernetes.io/de/docs/tasks/tools/install-kubectl/) installed
    * In case you use a separate management server copy the kubernetes config file to the management Server to connect to your cluster with kubectl ([copy config howto](https://stackoverflow.com/questions/40447295/how-to-configure-kubectl-with-cluster-information-from-a-conf-file))
* [Helm](https://helm.sh/) installed ([Helm installation](https://helm.sh/docs/intro/install/)), we are using HELM 3.x
* One or more Working PostgreSQL Server with Postgis extension
    * see [official Site](https://www.postgresql.org/) or [Ubuntu community site](https://help.ubuntu.com/community/PostgreSQL) for details
    * use version 12.x or later
    * [install postgis](https://postgis.net/install/)
* Connectivity between PostgreSQL and the Kubernetes Cluster (check/edit your pg_hba.conf)

## Setup steps

### Add Helm Repositories

* connect to the machine where your ```kubectl``` and ```helm``` client is installed
* add these Helm Repositories to your helm instance if not already there:

```bash
# stable repo channel
helm repo add stable https://charts.helm.sh/stable
# contains keycloak
helm repo add codecentric https://codecentric.github.io/helm-charts
# contains cert-manager
helm repo add jetstack https://charts.jetstack.io
# ingress-nginx controller
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
# contains the Urban Data Hub IoT chart
helm repo add hh-udp https://api.bitbucket.org/2.0/repositories/geowerkstatt-hamburg/hh-udp-iot/src/master/

# check if these repositories are added
helm repo list
# search for charts in a repo
helm search repo hh-udp/
#helm repository update
helm repo update
```

### Deploy the cert manager Helm Chart
To receive TLS-certificates automatically install cert-manager by following the steps provided here: https://cert-manager.io/docs/installation/
The first thing you'll need to configure after you've installed cert-manager is a ClusterIssuer or an Issuer in every namespace. 

#### Cluster-Issuer
Create cluster issuers for the prod and staging environment of let's encrypt.

```bash
kubectl apply -f manifests/clusterissuer_prod.yaml
kubectl apply -f manifests/clusterissuer_staging.yaml
```
By using the cluster-issuer there are problems with applications/certs in in different namespaces and more than one ingressclass.
Our recommendation is to use an issuer in every namespace. 

#### Issuer
Create an issuer for the prod environment of let's encrypt in every namespace. (frosta, frostb, keycloak and so on....)
Edit the manifest and change the mail,namespace and the ingress-class, than create the issuer: 

```bash
kubectl apply -f manifests/issuer_prod.yaml
```
In the frostserver and keycloak values file you have to use the issuerType: Issuer !

Example: 
```bash
cert:
  # ingress class annotation for all ingresses to bind to a specific ingress controller
  ingressClass: "tlf"
  # render the extra ca-cert template
  extra: true
  # enable templates ca-cert, ca-clusterissuer, route-ingress
  enabled: true
  # sets acme to staging or prod
  issuerType: Issuer
```

### Codecentric Keycloakx installation steps
#### Create and prepare a new database for Keycloakx
* connect to the server where your Postgresql database engine is installed

* switch to the postgres user with `sudo su postgres`

* Start the psql cli with `psql`, and run these queries (edit username and password):

```POSTGRESQL
CREATE USER YOUR_USER with encrypted password 'PASSWORD';
CREATE DATABASE keycloak ENCODING=UTF8 owner=YOUR_USER;
```

* Check the postgresql.conf for `listen_addresses = '*'` or modify it to reflect the IP range of your postgres server ([look here for details](https://www.postgresql.org/docs/current/runtime-config-connection.html))

* Check the  pg_hba.conf for an  appropriate setting that allows for connections via IPv4 from your kubernetes cluster
* example entry in pg_hba.conf:

  ```conf
  host keycloakuser keycloakDB 192.168.178.99/24 md5
  ```

* Verify that IPv4 connectivity is possible between your kubernetes cluster and the PostgeSQL server.
#### Copy an existing keycloak database for Keycloakx
* with the following psql commands the copy is working without stopping the source database  
* veryify the database names and the owner befor running the psql command

```POSTGRESQL
SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'keycloakdb'
AND pid <> pg_backend_pid();
CREATE DATABASE keycloakxdb
WITH TEMPLATE keycloakdb
OWNER udhkeycloak;
```
#### Create and prepare a IP and a DNS Name
* create a public static ip in azure in the same Resource group and location as the AKS Service 
* configure a dns name for this static ip (keycloakx public hostname)
#### Configure values files and configurations 
* download the files from /hh-udp-iot/manifests/keycloakx/*
* edit the following files where "changeme" is marked in the files 
  * 00_keycloakx-secrets.yaml -> insert the password for the database user to connect to the database
  * 01_keycloakx-issuer.yaml  -> insert mail for Let's Encrypt
  * 02_keycloakx_nginx_values.yaml -> insert loadbalancer ip (the public static ip)
  * 03_keycloakx_values.yaml -> insert dns host name and the database settings (hostname, user and database name)
#### Install a secret for the database password 
```Deploy
kubectl create ns keycloakx
kubectl apply -f 00_keycloakx-secrets.yaml
```
#### Install a Cert-Manager Issuer
```Deploy
kubectl apply -f 01_keycloakx-issuer.yaml
kubectl get issuers.cert-manager.io -A
```
#### Deploy NGINX Ingress Controller for Keycloak
```Deploy
helm install keycloakxnginxcontroller  ingress-nginx/ingress-nginx --version 4.2.5  -f 02_keycloakx_nginx_values.yaml  -n keycloakx
```
#### Deploy Keycloakx 
```Deploy
helm install keycloakx codecentric/keycloakx -n keycloakx -f 03_keycloakx_values.yaml
```
#### Keycloak Realm and Client configuration
* Open https://yourKeycloakFQDN/auth/ or your external Keycloak address in a webbrowser
* Log in with admin account
  * if it is a new database use the password from the values file and change it
  * if you copy the database use the existing admin account 
* Configure KeyCloak as shown in the KeyCloak setup description [keycloak-setup](keycloak-setup.md)

### FROST-Server setup and installation

#### Create and prepare a database for the FROST-Server

* connect to your database server
* switch to the postgres user with: `sudo su postgres`
* start the psql cli: `psql`
* run the following commands (insert your own user and password):

```SQL
-- create user
CREATE USER sensorthings WITH ENCRYPTED PASSWORD 'ChangeMe';
-- create database
CREATE DATABASE sensorthings ENCODING=UTF8 OWNER=sensorthings;
-- check if the database is there
\l
-- connect to your database
\c sensorthings
-- activate postgis extension for the FROST-Server database
CREATE EXTENSION postgis;
-- quit psql cli
\q
```

#### Deploy the HH-UDP-IoT Helm Chart

* download and edit the values.yaml with

```bash
curl -OL https://api.bitbucket.org/2.0/repositories/geowerkstatt-hamburg/helm-charts/src/master/hh-udp-iot/values.yaml
```

* or download it from the repository manually

You need to adjust at least these values to account for your environment and your specific needs. Before deploying the HH-UDP-IoT Helm Chart, you need to have an external IP adress with a DNS entry in your cloud environment. The exact procedure to obtain these ressource depends on your cloud provider.
There will be an external Frost-HTTP deployment and an internal Frost-HTTP deployment. The parameters have to changed in both deployments. The prupose of the second deployment is to separate incomming data from the publich read requests.
The deployment can be used to serve internet facing requests by a read-only database replica on a second databaseserver for example. The scaling and the performance of the pods and database servers can be individually optimized in both deployments.

Parameter  | Description | Default
------------- | ------------- | -------------
frost.serviceHost.fqdn | DNS entry for your Frost-Server |
frost.auth.keycloakConfigUrl | client registration endpoint of your FROST-Server (look [here for setup details](keycloak-setup.md)) | http://{your-keycloak-address}/realms/{REALMNAME}/clients-registrations/install/{CLIENTNAME}
frost.auth.keycloakConfigSecret | Insert the client secret here that you have  Keycloak config steps |
frost.db.ip | IP adress of your external postgres database server or multiple ip separated by "," if read replicas can be used. |
frost.db.frostConnection.database | name of the created FROST-Server database on the external postgres server | sensorthings
frost.db.frostConnection.username | usernamename of the created user for the FROST-Server database on the external postgres server | sensorthings
frost.db.frostConnection.password | password of the created user for the FROST-Server database on the postgres server |
frost.db.keycloakConnection.database | name of the created keycloak database on the postgres server | keycloak
frost.db.keycloakConnection.username | username of the created user for the keycloak database on the postgres server | keycloakuser
frost.db.keycloakConnection.password | password of the created user for the keycloak database on the postgres server |
nginx-frost.controller.service.type | (Depending on your environement for external access LoadBalancer) | Loadbalancer
nginx-frost.controller.service.loadBalancerIP | external static  IP of your cloud DNS for the FROST-Server, will be added to the Load balancer Ressource |
cert.email | email to get certificate information from let's encrypt |
cert.frost.extraTlsHosts | your FROST-Server DNS entry |
nginx-frost.tcp.1883 | service name and location of the mqtt-Service the Ingress Controller searches for, when redirecting requests to the mqtt-Pod, it`s name consists of {NAMESPACE}/{HELM-RELEASE}-{CHART.NAME}-mqtt:1883|default/frost-hh-udp-iot-mqtt:1883

**Note:**
The value for `nginx-frost.tcp.1883`-entry needs to be changed to account for the helm chart release name and the kubernetes namespace used, e.g. for `helm install -f values.yaml --name iot --namespace frost` it needs to be changed to "*iot/frost-hh-udp-iot-mqtt:1883*".

* do a dry run to check for errors

```bash
helm install frost -n frosta -f ./values.yaml . --debug --dry-run
```

* finally install

```bash
helm install frost -n frosta -f ./values.yaml . --debug
```

#### do some checks after the installation

```bash
# check if all services are up and running (getting the external IP's from the cloud loadbalancer may take a few minutes!)
kubectl get svc
# check if the two letsencrypt certificates are "True"
kubectl get certs -n frost
# check that the ingress ressources are created
kubectl get ingress -n frost
# check that all pods are Running
kubectl get pods -n frost
# check the logs of your http / mqtt / default backend pods for any errors
kubectl logs -f {POD-NAME}
```

### [Optional] Configure streaming-replication

To configure streaming replication between two postgres nodes do the following:

```bash
sudo su postgres
```

```SQL
-- create replication user (change the password)
CREATE USER rep REPLICATION LOGIN CONNECTION LIMIT 1 ENCRYPTED PASSWORD 'password';
```

* configure pg_hba.conf file:
  * add this to the file on master node

```config
host    replication     replication     172.16.0.6/32           md5
```

* find the PostgreSQL data directory on the master

```PostgreSQL
postgres=# SHOW data_directory;

```

* to make a backup of the master data directory on the replica, execute this command on the replica:


```bash
pg_basebackup --checkpoint=fast -h 172.16.0.5 -U replication -p 5432 -D /data/postgresql/12/ -Fp -Xs -P -R
```

* check if the files in /data/postgresql/12/ have the right "postgres" user.

* restart the replica database
* login on the master

```bash
sudo -u postgres psql
```

* check the streaming state:

```PostgreSQL
postgres=# SELECT client_addr, state FROM pg_stat_replication;
```

```bash
# Output should be:
   client_addr    |  state
------------------+-----------
 your_replica_IP | streaming
```

## Notes

### Lets Encrypt rate limits

If you have trouble aquiring certificates set the `cert.productionEnabled` to false to use the Let's Encrypt staging environment. Otherwise on multiple retries you probably will hit the LE rate limit (Failed Validation limit of 5 failures per account, per hostname, per hour on the LE). On the LE staging environment this limit is significantly higher.

### Local Helm chart deployment

If you deploy from a local folder instead of Helm repositories configure a .helmignore file to prevent parsing errors during the deployment. See [the helm docs](https://helm.sh/docs/chart_template_guide/helm_ignore_file/) (i.e. add .vscode for example).

### Known issues

* The entry for `nginx-frost.tcp.1883` in the values.yaml of this Helm chart won´t adapt to the installation parameters used with `helm install` using a custom namespace and relase name. You have to know these values before installing and enter as shown in [this Note](#note). Otherwise your mqtt Pods can´t be reached from outside of your cluster.
* Messages send over the MQTT protocol to and from the FROST-Server are currently not tls encrypted.

## Source and documentation pages

### About Hamburg Urban Data Platform

* [Urban Data Hub](https://www.hamburg.de/bsw/urban-data-hub/)
* [Urban Data Platform](http://www.urbandataplatform.hamburg/)
* [Integrating realtime data into Hamburg Urban Data Platform - zfv 1/2021 (german)](https://geodaesie.info/zfv/heftbeitrag/8651/zfv_2021_1_Fischer_et-al.pdf)
### General documentation sites

* [Kubernetes Ingress Documentation](https://kubernetes.github.io/ingress-nginx/deploy/)
* [Ingress-NGINX repository on Github](https://github.com/kubernetes/ingress-nginx)
* [Keycloak on Github](https://github.com/codecentric/helm-charts/tree/master/charts/keycloak)
* [Cert-manager installation Guide](https://cert-manager.io/docs/installation/kubernetes/#installing-with-helm)

### Keycloak REALM-configuration for FROST-Server

* [FROST-Server auth configuration](https://fraunhoferiosb.github.io/FROST-Server/auth.html)
* [FROST-Server auth configuration hints](https://github.com/FraunhoferIOSB/FROST-Server/issues/117)

## License information

Product  | more Info | Licence
-------- | --------- | --------
FROST-Server | [Fraunhofer IOSB](https://www.iosb.fraunhofer.de/servlet/is/80113/)  |[GNU](https://github.com/FraunhoferIOSB/FROST-Server/blob/master/LICENSE) |
Keycloak | [keycloak.org](https://www.keycloak.org/) | [Apache License 2.0](https://github.com/keycloak/keycloak/blob/master/LICENSE.txt)
Ingress-NGINX | [kubernetes/ingress-nginx](https://kubernetes.github.io/ingress-nginx/) | [Apache License 2.0](https://github.com/kubernetes/ingress-nginx/blob/master/LICENSE)
Cert-manager | [cert-manager.io](https://cert-manager.io/) | [Apache License 2.0](https://github.com/jetstack/cert-manager/blob/master/LICENSE)
